package br.com.ponto.repositories;

import br.com.ponto.models.Ponto;
import br.com.ponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface PontoRepository extends CrudRepository<Ponto, UUID> {
    ArrayList<Ponto> findAllByUsuario(Usuario usuario);
}
