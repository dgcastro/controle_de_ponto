package br.com.ponto.controllers;

import br.com.ponto.models.Ponto;
import br.com.ponto.models.Usuario;
import br.com.ponto.services.PontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;

@RestController
@RequestMapping("/pontos")
public class PontoController {
    @Autowired
    private PontoService pontoService;

    @PostMapping("/{id}")
    public Ponto RegistrarPonto(@PathVariable int id) {
        try {
            Ponto pontoSalvo = pontoService.salvarPonto(id);
            return pontoSalvo;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ArrayList<Ponto> buscarPorUsuario(@PathVariable int id){
        try{
            ArrayList<Ponto> pontos = pontoService.buscarPontosPorUsuario(id);
            return pontos;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
