package br.com.ponto.controllers;

import br.com.ponto.models.Usuario;
import br.com.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public Usuario incluirUsuario(@RequestBody Usuario usuario){
        return usuarioService.incluirUsuario(usuario);
    }

    @GetMapping("/{id}")
        public Usuario buscarPorId(@PathVariable int id){
        try{
            Usuario usuario = usuarioService.buscarUsuarioPorId(id);
            return usuario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
