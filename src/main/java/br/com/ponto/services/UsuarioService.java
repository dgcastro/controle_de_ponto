package br.com.ponto.services;

import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.UsuarioRepository;
import org.omg.CORBA.ShortSeqHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario) {
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Usuario incluirUsuario(Usuario usuario) {
        return salvarUsuario(usuario);
    }

    public Iterable<Usuario> buscarTodos() {
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }

    public Usuario buscarUsuarioPorId(int id) throws RuntimeException{
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        if(optionalUsuario.isPresent()){
            return optionalUsuario.get();
        }else{
            throw new RuntimeException("Não existe usuario com o Id informado.");
        }
    }

    public Usuario atualizarUsuario(int id, Usuario usuario) {
        if(usuarioRepository.existsById(id)){
            usuario.setId(id);
            return salvarUsuario(usuario);
        }else{
            throw new RuntimeException("Não existe usuario com o Id informado.");
        }
    }

    public void excluirUsuario(int id) {
        if(usuarioRepository.existsById(id)){
            usuarioRepository.deleteById(id);
        }else{
            throw new RuntimeException("Não existe usuario com o Id informado.");
        }
    }

}
