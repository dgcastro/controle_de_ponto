package br.com.ponto.services;

import br.com.ponto.enums.TipoDePontoEnum;
import br.com.ponto.models.Ponto;
import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.PontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

@Service
public class PontoService {
    @Autowired
    private PontoRepository pontoRepository;
    @Autowired
    private UsuarioService usuarioService;

    public Ponto salvarPonto(int id) {
        try{
            Usuario usuario = usuarioService.buscarUsuarioPorId(id);
            Ponto ponto = new Ponto();
            ponto.setUsuario(usuario);
            ponto.setDataDoRegistro(LocalDate.now());
            ponto.setHoraDoRegistro(LocalTime.now());
            ArrayList<Ponto> pontos = buscarPontosPorUsuario(id);

            if(!pontos.isEmpty()){
                if(pontos.get(pontos.size()-1).getTipoDePonto() == TipoDePontoEnum.ENTRADA){
                    ponto.setTipoDePonto(TipoDePontoEnum.SAIDA);
                }else {
                    ponto.setTipoDePonto(TipoDePontoEnum.ENTRADA);
                }
            }else{
                ponto.setTipoDePonto(TipoDePontoEnum.ENTRADA);
            }
            return pontoRepository.save(ponto);
        }catch(RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    public ArrayList<Ponto> buscarPontosPorUsuario(int id) {
        try{
            Usuario usuario = usuarioService.buscarUsuarioPorId(id);
            ArrayList<Ponto> pontos = pontoRepository.findAllByUsuario(usuario);
            return pontos;
        }catch(RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    public long calcularHorasTrabalhadas(int id) {
        long totalDeHoras = 0;
        try {
            Usuario usuario = usuarioService.buscarUsuarioPorId(id);
            ArrayList<Ponto> pontos = buscarPontosPorUsuario(id);
            if(!pontos.isEmpty()){
                for (int i = 0; i < pontos.size(); i++) {
                    if(pontos.get(i).getTipoDePonto() == TipoDePontoEnum.SAIDA){
                        totalDeHoras += Duration.between(pontos.get(i-1).getHoraDoRegistro(),
                                pontos.get(i).getHoraDoRegistro()).toMinutes();
                    }
                }
            }
            return totalDeHoras;
        }catch(RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
