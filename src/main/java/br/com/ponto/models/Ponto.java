package br.com.ponto.models;

import br.com.ponto.enums.TipoDePontoEnum;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

@Entity
@Table(name = "ponto")
public class Ponto {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column(name = "ponto_id")
    private UUID id;

    @Column(name = "date")
    private LocalDate dataDoRegistro;
    @Column(name = "hour")
    private LocalTime horaDoRegistro;
    @Column(name = "type")
    private TipoDePontoEnum tipoDePonto;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private Usuario usuario;


    public Ponto() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDate getDataDoRegistro() {
        return dataDoRegistro;
    }

    public void setDataDoRegistro(LocalDate dataDoRegistro) {
        this.dataDoRegistro = dataDoRegistro;
    }

    public LocalTime getHoraDoRegistro() {
        return horaDoRegistro;
    }

    public void setHoraDoRegistro(LocalTime horaDoRegistro) {
        this.horaDoRegistro = horaDoRegistro;
    }

    public TipoDePontoEnum getTipoDePonto() {
        return tipoDePonto;
    }

    public void setTipoDePonto(TipoDePontoEnum tipoDePonto) {
        this.tipoDePonto = tipoDePonto;
    }
}
