package br.com.ponto.models;

import org.hibernate.validator.constraints.br.CPF;

import java.time.LocalDate;
import java.util.List;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;


@Entity
@Table(name ="user")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @NotNull
    @Column(name = "name")
    private String nome;

    @CPF(message = "CPF incorreto")
    @NotNull
    @Column(name = "cpf")
    private String cpf;

    @Email(message = "e-mail incorreto")
    @NotNull
    @Column(name = "email")
    private String email;
    @NotNull
    @Column(name = "date")
    private LocalDate dataDeCadastro;

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataDeCadastro() {
        return dataDeCadastro;
    }

    public void setDataDeCadastro(LocalDate dataDeCadastro) {
        this.dataDeCadastro = dataDeCadastro;
    }
}
