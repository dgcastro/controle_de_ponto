package br.com.ponto.services;

import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {
    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Bobby");
        usuario.setCpf("54063422232");
        usuario.setEmail("bob@bobmail.com.br");
        usuario.setDataDeCadastro(LocalDate.parse("2017-01-03"));
    }

    @Test
    public void testarSalvarUsuario(){
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);
        Assertions.assertEquals(usuario, usuarioObjeto);
    }

    @Test
    public void testarIncluirUsuario(){
        Mockito.when(usuarioService.salvarUsuario(usuario)).thenReturn(usuario);
        Usuario usuarioObjeto = usuarioService.incluirUsuario(usuario);
        Assertions.assertEquals(usuarioObjeto, usuario);
    }

    @Test
    public void testarBuscarUsuarios(){
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);
        Iterable<Usuario> usuariosObjeto = usuarioService.buscarTodos();
        Assertions.assertEquals(usuarios, usuariosObjeto);
    }

    @Test
    public void testarBuscarUsuarioPorId(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(usuario));
        Usuario usuarioObjeto = usuarioService.buscarUsuarioPorId(1);
        Assertions.assertEquals(usuario, usuarioObjeto);
    }

    @Test
    public void testarBuscarUsuarioPorIdInexistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.buscarUsuarioPorId(404);});
    }

    @Test
    public void testarAtualizarUsuario(){
        Mockito.when(usuarioService.salvarUsuario(usuario)).thenReturn(usuario);
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Usuario usuarioObjeto = usuarioService.atualizarUsuario(1,usuario);
        Assertions.assertEquals(usuarioObjeto.getId(), usuario.getId());
    }

    @Test
    public void testarAtualizarUsuarioInexistente(){
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.atualizarUsuario(404,usuario);});
    }

    @Test
    public void testarExcluirHospede(){
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(true);
        usuarioService.excluirUsuario(1);
        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarExcluirHospedeInexistente(){
        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.excluirUsuario(404);});
    }

}
