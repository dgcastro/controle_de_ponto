package br.com.ponto.services;


import br.com.ponto.enums.TipoDePontoEnum;
import br.com.ponto.models.Ponto;
import br.com.ponto.models.Usuario;
import br.com.ponto.repositories.PontoRepository;
import br.com.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@SpringBootTest
public class PontoServiceTest {
    @MockBean
    private PontoRepository pontoRepository;
    @MockBean
    private UsuarioService usuarioService;
    @MockBean
    private UsuarioRepository usuarioRepository;
    @Autowired
    private PontoService pontoService;

    Usuario usuario;
    Ponto ponto;
    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Bobby");
        usuario.setCpf("54063422232");
        usuario.setEmail("bob@bobmail.com.br");
        usuario.setDataDeCadastro(LocalDate.parse("2017-01-03"));

        ponto = new Ponto();
        ponto.setId(UUID.randomUUID());
        ponto.setUsuario(usuario);
        ponto.setDataDoRegistro(LocalDate.now());
        ponto.setHoraDoRegistro(LocalTime.of(2,0,0));
        ponto.setTipoDePonto(TipoDePontoEnum.ENTRADA);

    }

    @Test
    public void testarSalvarPonto(){
        ArrayList<Ponto> pontos = new ArrayList<>();
        pontos.add(ponto);

        Mockito.when(usuarioService.buscarUsuarioPorId(Mockito.anyInt())).thenReturn(usuario);
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(ponto);
        Mockito.when(pontoService.buscarPontosPorUsuario(Mockito.anyInt())).thenReturn(pontos);

        Ponto pontoObjeto = pontoService.salvarPonto(usuario.getId());
        Assertions.assertEquals(ponto.getId(), pontoObjeto.getId());
    }

    @Test
    public void testarBuscarPontosPorUsuario(){
        ArrayList<Ponto> pontos = new ArrayList<>();
        pontos.add(ponto);
        Mockito.when(pontoRepository.findAllByUsuario(usuario)).thenReturn(pontos);
        Mockito.when(usuarioService.buscarUsuarioPorId(usuario.getId())).thenReturn(usuario);
        ArrayList<Ponto> pontosTeste = pontoService.buscarPontosPorUsuario(usuario.getId());
        Assertions.assertEquals(pontosTeste, pontos);
    }

    @Test
    public void testarCalcularHorasTrabalhadas(){
        ArrayList<Ponto> pontos = new ArrayList<>();
        pontos.add(ponto);
        Ponto pontoSaida = new Ponto();
        pontoSaida.setId(UUID.randomUUID());
        pontoSaida.setUsuario(usuario);
        pontoSaida.setDataDoRegistro(LocalDate.now());
        pontoSaida.setHoraDoRegistro(LocalTime.of(3,0,0));
        pontoSaida.setTipoDePonto(TipoDePontoEnum.SAIDA);
        pontos.add(pontoSaida);

        Mockito.when(usuarioService.buscarUsuarioPorId(Mockito.anyInt())).thenReturn(usuario);
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(ponto);
        Mockito.when(pontoService.buscarPontosPorUsuario(Mockito.anyInt())).thenReturn(pontos);
        Assertions.assertEquals(Duration.between(ponto.getHoraDoRegistro(),pontoSaida.getHoraDoRegistro()).toMinutes(),
                pontoService.calcularHorasTrabalhadas(usuario.getId()));
    }

}
