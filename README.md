localhost:8080/usuarios
    post
        body:
            {
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
    
    return 200
    {
        "id": 1,
        "nome": "Bobby",
        "cpf": "54063422232",
        "email": "bobby@bobemail.com.br",
        "dataDeCadastro": "2017-01-03"
    }

localhost:8080/usuarios/1
    get
    return 200
    {
        "id": 1,
        "nome": "Bob",
        "cpf": "54063422232",
        "email": "bobby@bobemail.com.br",
        "dataDeCadastro": "2017-01-03"
    }

localhost:8080/pontos/1
    (observação: para registrar um ponto nao é necessário nenhum conteudo,
        apenas passar um usuário válido na rota.
        O serviço define se é uma entrada ou saída, baseado no ultimo registro).
    post 
    return 200
    {
        "id": "e3ccad74-ab83-42b6-a93d-aaefe0363b11",
        "dataDoRegistro": "2020-07-07",
        "horaDoRegistro": "05:29:47.407",
        "tipoDePonto": "ENTRADA",
        "usuario": {
            "id": 1,
            "nome": "Bobby",
            "cpf": "54063422232",
            "email": "bobby@bobemail.com.br",
            "dataDeCadastro": "2017-01-03"
        }
    }
    
    get
    return 200
    [
        {
            "id": "944259af-70ae-4262-a581-4fa0861b3f90",
            "dataDoRegistro": "2020-07-07",
            "horaDoRegistro": "05:29:27",
            "tipoDePonto": "ENTRADA",
            "usuario": {
                "id": 1,
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
        },
        {
            "id": "53fcfa8a-9564-4537-a9ec-36b7fc0b3544",
            "dataDoRegistro": "2020-07-07",
            "horaDoRegistro": "05:29:35",
            "tipoDePonto": "SAIDA",
            "usuario": {
                "id": 1,
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
        },
        {
            "id": "e09e4aa6-d5e3-4adf-995e-a344bacbc505",
            "dataDoRegistro": "2020-07-07",
            "horaDoRegistro": "05:29:40",
            "tipoDePonto": "ENTRADA",
            "usuario": {
                "id": 1,
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
        },
        {
            "id": "256a40e9-3ec2-4560-8a07-24375cc93b10",
            "dataDoRegistro": "2020-07-07",
            "horaDoRegistro": "05:29:42",
            "tipoDePonto": "SAIDA",
            "usuario": {
                "id": 1,
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
        },
        {
            "id": "c61ca887-427e-4c44-9a36-3440e364a9f0",
            "dataDoRegistro": "2020-07-07",
            "horaDoRegistro": "05:29:43",
            "tipoDePonto": "ENTRADA",
            "usuario": {
                "id": 1,
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
        },
        {
            "id": "40d4f584-ab36-4df7-9b86-9919d3a1bc80",
            "dataDoRegistro": "2020-07-07",
            "horaDoRegistro": "05:29:46",
            "tipoDePonto": "SAIDA",
            "usuario": {
                "id": 1,
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
        },
        {
            "id": "e3ccad74-ab83-42b6-a93d-aaefe0363b11",
            "dataDoRegistro": "2020-07-07",
            "horaDoRegistro": "05:29:47",
            "tipoDePonto": "ENTRADA",
            "usuario": {
                "id": 1,
                "nome": "Bobby",
                "cpf": "54063422232",
                "email": "bobby@bobemail.com.br",
                "dataDeCadastro": "2017-01-03"
            }
        }
    ]